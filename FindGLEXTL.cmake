find_path(GLEXTL_INCLUDE_DIR GL/glextl.h
	PATH_SUFFIXES include
)

mark_as_advanced(GLEXTL_INCLUDE_DIR)

if(GLEXTL_INCLUDE_DIR)
    set(GLEXTL_FOUND TRUE)
endif()

if(GLEXTL_FOUND)
    if(NOT GLEXTL_FIND_QUIETLY)
        message(STATUS "Found GLEXTL")
    endif()
else()
    if(GLEXTL_FIND_REQUIRED)
        message(FATAL_ERROR "Could not find GLEXTL")
    endif()
endif()
