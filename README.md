# glextloader #

This CMake project will download opengl extension headers and create extension loading code for it. The easiest way to use this is to make it a submodule and add its include folder to your include directories. From there you'll have all opengl headers you will need.